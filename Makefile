run: compile exec clear
	echo "Done."

compile:
	echo "Compiling..."
	gcc "$(target)" -Wall

exec:
	echo "Executing..."
	./a.out

clear:
	rm *.out

.PHONY: run compile exec clear
