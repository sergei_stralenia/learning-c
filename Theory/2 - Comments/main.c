#include<stdio.h>

int main()
{
  // This is line comment. From "//" till end of string is comment.

  /*
    This is block comment. From "/*" till "*\/" is comment.
  */

  //printf("Number = %d\n",// 5);
  printf("Number = %d\n",/* This comment will be ignored*/ 5);
  
  return 0;
}