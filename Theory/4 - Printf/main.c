#include<stdio.h>

int main()
{
  // int printf(char[] format, argr arg2, ...)
  // returns count of printed characters
  // Format: any_string + (% + modifier) + any_string
  // Example: "Total result: %d, and amout: %f. Done."

  // MODIFIERS
  // Modifier d, i - integer numbers
  int d = 5;
  printf("%%d: %d\n", d);
  printf("%%i: %d\n", d);

  // Modifier o - octal number
  int o = 9;
  printf("%%o: %o\n", o);

  // Modifier x, X - hex number
  int x = 125;
  printf("%%x: %x\n", x);

  // Modifier u - unsigned number
  int u = 5;
  printf("%%u: %u\n", u);

  // Modifier c - character
  char c = 'a';
  printf("%%c: %c\n", c);

  // Modifier s - string
  char s[] = "abcd";
  printf("%%s: %s\n", s);

  // Modifier f - float
  float f = 4.675;
  printf("%%f: %f\n", f);

  // Modifier e,E - double
  double e = 4.6748345;
  printf("%%e: %e\n", e);

  // FORMATERS
  // Formater: adjustments
  int adjustment = 5;
  printf("%%5d: %5d\n", adjustment);

  // Formatter: left adjustment
  int left_adjustment = 5;
  printf("%%-5d: %-5d\n", left_adjustment);

  // Formatter: precision
  float precision = 1.23456789;
  printf("%%.3f: %.3f\n", precision);

  return 0;
}