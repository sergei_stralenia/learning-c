#include<stdio.h>

int main()
{
  // type variable_name[=value];
  // type variable_name[=value], variable_name[=value];

  // Types: char, int, float, double
  char a;
  int b = 3, c;
  float d = 3.15;
  double e;

  a = 12;
  e = 3.15e-2;

  printf("Size: %lu, Value %d\n", sizeof(a), a);
  printf("Size: %lu, Value %d\n", sizeof(b), b);
  printf("Size: %lu, Value %f\n", sizeof(d), d);
  printf("Size: %lu, Value %lf\n", sizeof(e), e);

  // Size modificators: short, long

  long int f = 124;
  short g;

  g = 3;

  printf("Size: %lu, Value %ld\n", sizeof(f), f);
  printf("Size: %lu, Value %d\n", sizeof(g), g);

  // Sign modificators: signed (default), unsigned;

  unsigned int h = -215;
  signed short int i = 523344;

  printf("Size: %lu, Value %lu\n", sizeof(h), h);
  printf("Size: %lu, Value %d\n", sizeof(i), i);

  // Multiple equals

  int a1, a2, a3;

   a1 = a2 = a3 = 0;

  // Variable name: [0-9], _, [a-z], [A-Z]
  // Do not use _ as first symbol (used in system libaries)

  int t_eT9s = 12;

  printf("%d\n", t_eT9s);

  // Contansts
  const int var = 3;
  // Error!: var = 5;

  return 0;
}